#!/bin/bash


DIR="${PWD}"
USER_SCRIPT=$USER
args=("$@")
# Fonctions ###########################################################

help_list() {
  echo "Usage:

  ${0##*/} [-h][--prometheus][--grafana]

  Options:

    -h, --help
      affiche les commandes possible

    -a, --apapche
      lance un conteneur apache
      
    -c, --cloner
      lance un cloner git depuis un conteneur 
      
    -ca, --cassandra
      lance un conteneur cassandra
      
    -d, --drupal
      lance un conteneur drupal

    -g, --gitlab
      run gitlab

    -gra, --grafana
      lance un conteneur grafana

    -i, --ip
      liste les addresse ip pour chaque conteneur

    -in, --init
      reinitialise tous à zero (volumes,network,container)

    -j, --jenkins
      lance un conteneur jenkins

    -joo, --joomla
      lance un conteneur joomla

    -k, --kafka
      lance un conteneur kafka avec zookeeper
    
    -l, --lamp
      lance un conteneur avec un serveur lamp

    -le,--lemp
      lance un conteneur avec un serveur lemp

    -ma, --mariadb
      lance un conteneur mariadb

    -mo, --mongodb
      lance un conteneur mongodb

    -my, --mysql
      lance un conteneur mysql

    -n, --nginx
      lance un conteneur nginx

    -pma, --phpmyadmin
      lance un conteneur phpmyadmin
      pour uttiliser faire : ./${0##*/} -pma nom_du_container_BDD_a_lier

    -po, --portainer
      lance un conteneur portainer

    -pos, --postgres
      lance un conteneur postgres

    -pro, --prometheus
      lance un conteneur prometheus

    -r, --remove
      supprime tout les container
      réalise un docker ps -aq | xargs docker rm

    -ri, --remove-images
      supprime tout les images
      réalise un docker images -q | xargs docker rmi

    -rn, --remove-network
      si ERROR: Pool overlaps with other one on this address space
      supprime tous les réseaux
      docker network ls -q | xargs docker network rm

    -rv, --remove-volumes
      si ERROR: Pool overlaps with other one on this address space
      supprime tous les volumes
      docker volumes ls -q | xargs docker volumes rm

    -t, --traefik
      lance un conteneur traefik

    -s, --stop 
      stop tous les conteneur en cours
      with docker ps -q | xargs docker stop
      
    -st, --strapi
      lance un conteneur strapi

    -sy, --symfony 
      lance un conteneur symfony avec lamp
    
    -w, --wordpress
      lance un conteneur wordpress
    
    -y, --yunohost
      lance un conteneur yunohost
    "
}

parse_options() {
  case ${args[0]} in
    -h|--help)
      help_list
      exit
      ;;
    -a|--apache)
      apache
      ;;
    -c|--cloner)
      cloner
      ;;
    -ca|--cassandra)
      cassandra
      ;;
    -d|--drupal)
      drupal
      ;;
    -de|--denoRust)
      deno_ssvm_rust
      ;;
    -g|--gitlab)
      gitlab
      ;;
    -gra|--grafana)
      grafana
      ;;
    -i|--ip)
      ip
      ;;
    -in|--init)
      init
      ;;
    -j|--jenkins)
      jenkins
      ;;
    -joo|--joomla)
      joomla
      ;;
    -k|--kafka)
      kafka
      ;;
    -l|--lamp)
      lamp
      ;;
    -le|--lemp)
      lemp
      ;;
    -ma|--mariadb)
      mariadb
      ;;
    -mo|--mongodb)
      mongodb
      ;;
    -my|--mysql)
      mysql
      ;;
    -n|--nginx)
      nginx
      ;;
    -pma|--phpmyadmin)
      phpmyadmin
      ;;
    -po|--portainer)
      portainer
      ;;
    -pos|--postgres)
      postgres
      ;;
    -pro|--prometheus)
      prometheus
      ;;
    -r|--remove-containers)
      remove-containers
      ;;
    -ri|--remove-images)
      remove-images
      ;;
    -rn|--remove-networks)
      remove-networks
      ;;
    -rv|--remove-volumes)
      remove-volumes
      ;;
    -s|--stop)
      stop
      ;;
    -st|--strapi)
      strapi
      ;;
    -sub|--subnet)
      subnet
      ;;
    -sy|--symfony)
      symfony
      ;;
    -t|--traefik)
      traefik
      ;;
    -w|--wordpress)
      wordpress
      ;;
    -y|--yunohost)
      yunohost
      ;;

    *)
      echo "Option inconnu: ${opt} - lancer la commande ' ${0##*/} -h ' pour afficher les commandes possible.">&2
      exit 1
  esac
}

apache() {
  
  appname=apache
  check

  clear
  echo
  echo " Entrez un nom de domaine ex : exemple.fr"
  read domain

  echo
  echo "Install $appname-$id"

  echo "1 - Create directories ./$appname-$id/"
  mkdir -p $DIR/$appname-$id/{data,certs,vhosts,conf}

echo " Hello World " > $DIR/$appname-$id/data/index.html

 echo "3 - Create docker-compose file"
echo "
version: '3.6'
services:
  $appname-$id:
    image: httpd:latest
    container_name: $appname-$id
    ports:
      - $port80:80
      - $port443:443
    volumes:
      - $appname-$id-data:/usr/local/apache2/htdocs/
      - $appname-$id-certs:/certs
      - $appname-$id-conf/:/usr/local/apache2/conf/
    labels:
      - traefik.enable=true
      - traefik.http.routers.$appname-$id-frontend.rule=Host(\`$appname-$id.$domain\`)
      - traefik.http.routers.$appname-$id-frontend.entrypoints=secureweb
      - traefik.http.services.$appname-$id-frontend.loadbalancer.server.port=80
      - traefik.http.routers.$appname-$id-frontend.service=$appname-$id-frontend
      - traefik.http.routers.$appname-$id-frontend.tls.certresolver=cert
    networks:
     - frontend
volumes:
  $appname-$id-data:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: ./$appname-$id/data
  $appname-$id-certs:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: ./$appname-$id/certs
  $appname-$id-conf:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: ./$appname-$id/conf
networks:
  frontend:
    external:
      name: $name_subnet
" > $DIR/$appname-$id/docker-compose.yml

  echo "3 - Run $appname-$id "
  docker-compose -f $DIR/$appname-$id/docker-compose.yml up -d

  ip
}

cassandra() {
  
  appname=cassandra
  check
  clear
  echo
  echo "Install $appname-$id"
  
  echo "1 - Create directories ./$appname-$id/"
  mkdir -p $DIR/$appname-$id/

 echo "2 - Create docker-compose file"
echo "
version: '3.6'
services:
  $appname-$id:
    image: bitnami/$appname:latest
    container_name: $appname-$id
    volumes:
      - $appname-$id-data:/bitnami
    ports:
      - 9042:9042 # cqlsh
      - 7199:7199 # jmx
      - 7000:7000 # internode communication
      - 7001:7001 # tls internode
      - 9160:9160 # client api
    networks:
      - frontend
volumes:
  $appname-$id-data:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: ./$appname-$id/
networks:
  frontend:
    external:
      name: $name_subnet
" >$DIR/$appname-$id/docker-compose.yml

  echo "3 - Run $appname-$id"
  docker-compose -f $DIR/$appname-$id/docker-compose.yml up -d

  echo "4 - Credentials"
  echo "
  Usage :
  docker exec -i $appname-$id cqlsh -u cassandra -p cassandra
                  user: cassandra
                  password: cassandra
  "
  ip
}

check() {
  checksubnet
  checkid
  checkport
}

checkid() {
  clear
  echo
  echo "Traitement en cours..."
  echo
  echo "check id"
  idmax=`docker ps -a --format '{{ .Names}}' | awk -F "-" -v app="$appname" '$0 ~ app"-" {print $2}' | sort -r |head -1`
  if [ ! -z "$idmax" ]; then 
      clear
      echo
      echo "Un ou plusieur conteneur existe déja, voulez-vous en lancer un existant ou en créer un nouveau ? ";
      echo
      echo "Pour lancer un conteneur déja existant colle le nom d'un des conteneurs qui s'affiche tout en haut ex:yunohost-1"
      echo
      docker ps -a --format '{{ .Names}}' | awk -F "-" -v app="$appname" '$0 ~ app"-" {print $0}' | sort -r
      echo
      echo "sinon appuiez sur entrer..."
      read app
        if [ ! -z "$app" ]; then 
        docker-compose -f $DIR/$app/docker-compose.yml up -d
        exit
      else id=$(($idmax + 1));
      fi
  else id=$(($idmax + 1));
  fi
}

checkport() {
  clear
  echo
  echo "Traitement en cours..."
  echo
  echo "check ports"
 port22=$(for i in $(docker ps -q); do docker port $i 22 2>/dev/null | awk -F ":" '{ $1 = "" ; print $0 }'; done | sort -r | head -1 )
 port25=$(for i in $(docker ps -q); do docker port $i 25 2>/dev/null | awk -F ":" '{ $1 = "" ; print $0 }'; done | sort -r | head -1 )
 port53udp=$(for i in $(docker ps -q); do docker port $i 53/udp 2>/dev/null | awk -F ":" '{ $1 = "" ; print $0 }'; done | sort -r | head -1 )
 port80=$(for i in $(docker ps -q); do docker port $i 80 2>/dev/null | awk -F ":" '{ $1 = "" ; print $0 }'; done | sort -r | head -1 )
 port443=$(for i in $(docker ps -q); do docker port $i 443 2>/dev/null | awk -F ":" '{ $1 = "" ; print $0 }'; done | sort -r | head -1 )
 port137=$(for i in $(docker ps -q); do docker port $i 137 2>/dev/null | awk -F ":" '{ $1 = "" ; print $0 }'; done | sort -r | head -1 )
 port138=$(for i in $(docker ps -q); do docker port $i 138 2>/dev/null | awk -F ":" '{ $1 = "" ; print $0 }'; done | sort -r | head -1 )
 port139=$(for i in $(docker ps -q); do docker port $i 139 2>/dev/null | awk -F ":" '{ $1 = "" ; print $0 }'; done | sort -r | head -1 )
 port465=$(for i in $(docker ps -q); do docker port $i 465 2>/dev/null | awk -F ":" '{ $1 = "" ; print $0 }'; done | sort -r | head -1 )
 port587=$(for i in $(docker ps -q); do docker port $i 587 2>/dev/null | awk -F ":" '{ $1 = "" ; print $0 }'; done | sort -r | head -1 )
 port993=$(for i in $(docker ps -q); do docker port $i 993 2>/dev/null | awk -F ":" '{ $1 = "" ; print $0 }'; done | sort -r | head -1 )
 port1337=$(for i in $(docker ps -q); do docker port $i 1337 2>/dev/null | awk -F ":" '{ $1 = "" ; print $0 }'; done | sort -r | head -1 )
 port3000=$(for i in $(docker ps -q); do docker port $i 3000 2>/dev/null | awk -F ":" '{ $1 = "" ; print $0 }'; done | sort -r | head -1 )
 port3306=$(for i in $(docker ps -q); do docker port $i 3306 2>/dev/null | awk -F ":" '{ $1 = "" ; print $0 }'; done | sort -r | head -1 )
 port5000=$(for i in $(docker ps -q); do docker port $i 5000 2>/dev/null | awk -F ":" '{ $1 = "" ; print $0 }'; done | sort -r | head -1 )
 port5222=$(for i in $(docker ps -q); do docker port $i 5222 2>/dev/null | awk -F ":" '{ $1 = "" ; print $0 }'; done | sort -r | head -1 )
 port5432=$(for i in $(docker ps -q); do docker port $i 5432 2>/dev/null | awk -F ":" '{ $1 = "" ; print $0 }'; done | sort -r | head -1 )
 port5269=$(for i in $(docker ps -q); do docker port $i 5269 2>/dev/null | awk -F ":" '{ $1 = "" ; print $0 }'; done | sort -r | head -1 )
 port8080=$(for i in $(docker ps -q); do docker port $i 8080 2>/dev/null | awk -F ":" '{ $1 = "" ; print $0 }'; done | sort -r | head -1 )
 port9000=$(for i in $(docker ps -q); do docker port $i 9000 2>/dev/null | awk -F ":" '{ $1 = "" ; print $0 }'; done | sort -r | head -1 )
 port9091=$(for i in $(docker ps -q); do docker port $i 9091 2>/dev/null | awk -F ":" '{ $1 = "" ; print $0 }'; done | sort -r | head -1 )
 port27017=$(for i in $(docker ps -q); do docker port $i 27017 2>/dev/null | awk -F ":" '{ $1 = "" ; print $0 }'; done | sort -r | head -1 )
 port49200=$(for i in $(docker ps -q); do docker port $i 49200 2>/dev/null | awk -F ":" '{ $1 = "" ; print $0 }'; done | sort -r | head -1 )

if [ -z "$port22" ]; then port22=22 ; else port22=$(($port22 + 1)); fi
if [ -z "$port25" ]; then port25=25 ; else port25=$(($port25 + 1)); fi
if [ -z "$port53udp" ]; then port53udp=53 ; else port53udp=$(($port53udp + 1)); fi
if [ -z "$port80" ]; then port80=80 ; else port80=$(($port80 + 1)); fi
if [ -z "$port137" ]; then port137=137 ; else port137=$(($port137 + 3)); fi
if [ -z "$port138" ]; then port138=138 ; else port138=$(($port138 + 3)); fi
if [ -z "$port139" ]; then port139=139 ; else port139=$(($port139 + 3)); fi
if [ -z "$port443" ]; then port443=443 ; else port443=$(($port443 + 3)); fi
if [ -z "$port465" ]; then port465=465 ; else port465=$(($port465 + 1)); fi
if [ -z "$port587" ]; then port587=587 ; else port587=$(($port587 + 1)); fi
if [ -z "$port993" ]; then port993=993 ; else port993=$(($port993 + 1)); fi
if [ -z "$port1337" ]; then port1337=1337 ; else port1337=$(($port1337 + 1)); fi
if [ -z "$port3000" ]; then port3000=3000 ; else port3000=$(($port3000 + 1)); fi
if [ -z "$port3306" ]; then port3306=3306 ; else port3306=$(($port3306 + 1)); fi
if [ -z "$port5000" ]; then port5000=5000 ; else port5000=$(($port5000 + 1)); fi
if [ -z "$port5222" ]; then port5222=5222 ; else port5222=$(($port5222 + 1)); fi
if [ -z "$port5269" ]; then port5269=5269 ; else port5269=$(($port5269 + 1)); fi
if [ -z "$port5432" ]; then port5432=5432 ; else port5432=$(($port5432 + 1)); fi
if [ -z "$port8080" ]; then port8080=8080 ; else port8080=$(($port8080 + 1)); fi
if [ -z "$port9000" ]; then port9000=9000 ; else port9000=$(($port9000 + 1)); fi
if [ -z "$port9091" ]; then port9091=9091 ; else port9091=$(($port9091 + 1)); fi
if [ -z "$port27017" ]; then port27017=27017 ; else port27017=$(($port27017 + 1)); fi
if [ -z "$port49200" ]; then port49200=49200 ; else port49200=$(($port49200 + 1)); fi
}

checksubnet() {
  clear
  echo
  echo "Traitement en cours..."
  echo
  echo "check sous-réseaux"
  listNetwork=`docker network ls -f type=custom --format "{{.Name}}: {{.Driver}}"`
  if [ ! -z "$listNetwork" ]; then
    clear
    echo
    echo "Un ou plusieurs réseaux existe déja, voulez-vous en uttiliser un existant ou créer un nouveau réseau ? ";
    echo
    echo "Si aucun réseaux n'est selectionné ni créé alors le réseau 'frontend' sera uttiliser par deault"
    echo
    echo "Pour utiliser un réseau déja existant autre que le réseau par défaut, collez le nom du reseaux qui s'affiche ex : frontend"
    echo
    echo $listNetwork
    echo
    echo "sinon appuiez sur entrer..."
    read name_subnet
    if [ ! -z "$network" ]; then
      continue
    else 
      subnet
    fi
  else 
    subnet
  fi
}
subnet() {
  clear
    echo "le conteneur $appname à besoin d'un réseaux afin d'acceder au conteneur depuis un navigateur."
    echo
    echo "un réseaux par défaut 'frontend' sera créé si vous n'en créez pas un et si il n'existe pas"
    echo
    echo "Voulez vous créer un réseaux personalisé ? : répondre avec la touche 'O' ou laisser par défaut en appuyant sur n'importe quel autre touche."
    read choice
    if [[ "$choice" ==  [oO] ]]; then
      echo "Veuillez saisir un nom de réseaux ex: frontend"
      read name_subnet
      echo "Veuillez saisir un sous-réseaux ip ex: 172.26.1.0 (un sous-réseau fini toujours par .0)"
      until [[ ${subnet} =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0]$ ]]; do
        read subnet
      done
      docker network create --subnet=$subnet/24 $name_subnet
      docker network inspect $name_subnet -f "{{range .IPAM.Config}}{{.Subnet}}{{end}} - {{.Name}}"
    else
     echo "Le réseaux frontend sera créer par défaut"
      name_subnet=frontend
      docker network create $name_subnet
      docker network inspect $name_subnet -f "{{range .IPAM.Config}}{{.Subnet}}{{end}} - {{.Name}}"
      sleep 1s
    fi
}
cloner() {
  
  appname=cloner
  checkid
  clear
  echo
  echo "Ceci est un téléchargeur de depot git et permet de mettre a jour le contenu d'un dossier existant avec la commande ' docker start '"
  echo "la premiere uttilisation necesite l'instalation d'une image docker et donc beaucoup plus lente"
  echo 
  echo " Entrez une url"
  read url
  echo " Entrez un nom de dossier"
  read folder

  echo
  echo "Install $appname-$id"

  echo "1 - Create directories ./$appname-$id/$folder"
  mkdir -p $DIR/$appname-$id/$folder

  echo "$url -> $folder
pour mettre à jour ou retélécharger le dossier $folder
faire la commande :
docker start $appname-$id" > $DIR/$appname-$id/infos.txt

  echo $folder
 echo "2 - Create docker-compose file"
echo "
version: '3.6'
services:
  $appname-$id:
    image: 'bitnami/git:latest'
    container_name: $appname-$id
    command:
      - clone
      - $url
      - $folder
    volumes:
      - $appname-$id-data:/$folder
volumes:
  $appname-$id-data:
    name: $appname-$id-data
    driver: local
    driver_opts:
      o: bind
      type: none
      device: ./$appname-$id/$folder
" >$DIR/$appname-$id/docker-compose.yml

  echo "3 - Run $appname-$id"
  docker-compose -f $DIR/$appname-$id/docker-compose.yml up -d

  echo "pour mettre à jour ou retelecharger le dossier $folder
  docker start $appname-$id"
}

drupal() {
  
  appname=drupal
  check
  port80Bis=$(($port80 + 1))

  clear
  echo
  echo " Entrez un nom de domaine ex : exemple.fr"
  read domain
  
  echo
  echo "Install $appname-$id"

  echo "1 - Create directories ./$appname-$id/"
  mkdir -p $DIR/$appname-$id/{data,db}
  mkdir -p $DIR/$appname-$id/data/{modules,profiles,themes,sites}

echo "2 - Create docker-compose file"
echo "
version: '3.6'
services:
  $appname-$id-db:
    container_name: $appname-$id-db
    image: mariadb
    restart: always
    volumes:
      - $appname-$id-db:/var/lib/mysql/
    ports:
      - $port3306:3306
    environment:
      MYSQL_ROOT_PASSWORD: rootpassword
      MYSQL_DATABASE: $appname-$id
      MYSQL_USER: $appname
      MYSQL_PASSWORD: $appname
    networks:
      - $appname-$id
  $appname-$id:
    container_name: $appname-$id
    image: drupal:8-apache
    ports:
      - $port80:80
    volumes:
      - $appname-$id-modules:/var/www/html/modules
      - $appname-$id-profiles:/var/www/html/profiles
      - $appname-$id-themes:/var/www/html/themes
      - $appname-$id-sites:/var/www/html/sites
    restart: always
    labels:
      - traefik.enable=true
      - traefik.http.routers.$appname-$id-frontend.rule=Host(\`$appname-$id.$domain\`)
      - traefik.http.routers.$appname-$id-frontend.entrypoints=secureweb
      - traefik.http.services.$appname-$id-frontend.loadbalancer.server.port=80
      - traefik.http.routers.$appname-$id-frontend.service=$appname-$id-frontend
      - traefik.http.routers.$appname-$id-frontend.tls.certresolver=cert
    networks:
      - $appname-$id
      - frontend 
  $appname-$id-myadmin:
    depends_on:
      - $appname-$id-db
    container_name: $appname-$id-myadmin
    image: phpmyadmin
    restart: always
    ports:
      - $port80Bis:80
    environment:
      PMA_HOST: $appname-$id-db
      MYSQL_ROOT_PASSWORD: rootpassword 
    labels:
      - traefik.enable=true
      - traefik.http.routers.$appname-$id-myadmin-frontend.rule=Host(\`$appname-$id-myadmin.$domain\`)
      - traefik.http.routers.$appname-$id-myadmin-frontend.entrypoints=secureweb
      - traefik.http.services.$appname-$id-myadmin-frontend.loadbalancer.server.port=80
      - traefik.http.routers.$appname-$id-myadmin-frontend.service=$appname-$id-myadmin-frontend
      - traefik.http.routers.$appname-$id-myadmin-frontend.tls.certresolver=cert
    networks:
      - $appname-$id
      - frontend
volumes:
  $appname-$id-sites:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: ./$appname-$id/data/sites
  $appname-$id-themes:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: ./$appname-$id/data/themes
  $appname-$id-profiles:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: ./$appname-$id/data/profiles
  $appname-$id-modules:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: ./$appname-$id/data/modules
  $appname-$id-db:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: ./$appname-$id/db
networks:
  $appname-$id:
    name: $appname-$id
  frontend:
    external:
      name: $name_subnet
" >$DIR/$appname-$id/docker-compose.yml

  echo "3 - Run $appname-$id"
  docker-compose -f $DIR/$appname-$id/docker-compose.yml up -d

  echo "
# Drupal with Mariadb
#
# During initial Drupal setup,
# Database type: Mariadb
# Database name: $appname-$id
# Database username: $appname
# Database password: $appname
# OPTIONS Avancé : 
# Database hote: $appname-$id-db
# Numéro de port: $port3306"

ip
}

gitlab() {
  
  appname=gitlab
  check

  clear
  echo
  echo " Entrez un nom de domaine ex : exemple.fr"
  read domain
  
  echo
  echo "Install $appname-$id"

  echo "1 - Creation dosssier ./$appname-$id/"
  mkdir -p $DIR/$appname-$id/{config,data,logs}

echo "2 - Create docker-compose file"
echo "
version: '3.6'
services:
  $appname-$id:
    image: '$appname/$appname-ce:latest'
    container_name: $appname-$id
    restart: always
    hostname: '$appname-$id.$domain'
    environment:
      GITLAB_OMNIBUS_CONFIG: |
        external_url 'https://$appname-$id.$domain'
        # Add any other $appname.rb configuration here, each on its own line
    ports:
      - $port80:80
      - $port443:443
      - $port5000:5000
    volumes:
      - $appname-$id-config:/etc/$appname
      - $appname-$id-logs:/var/log/$appname
      - $appname-$id-data:/var/opt/$appname
    labels:
      - traefik.enable=true
      - traefik.http.routers.$appname-$id-frontend.rule=Host(\`$appname-$id.$domain\`)
      - traefik.http.routers.$appname-$id-frontend.entrypoints=secureweb
      - traefik.http.services.$appname-$id-frontend.loadbalancer.server.port=80
      - traefik.http.routers.$appname-$id-frontend.service=$appname-$id-frontend
      - traefik.http.routers.$appname-$id-frontend.tls.certresolver=cert
    networks:
      - frontend     
volumes:
  $appname-$id-data:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: ./$appname-$id/data
  $appname-$id-logs:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: ./$appname-$id/logs
  $appname-$id-config:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: ./$appname-$id/config
networks:
  frontend:
    external:
      name: $name_subnet
" >$DIR/$appname-$id/docker-compose.yml

  echo "3 - Run $appname-$id"
  docker-compose -f $DIR/$appname-$id/docker-compose.yml up -d


  ip
  echo "Pour ouvrir $appname-$id.$domain depuis le navigateur
Ajouter ip de $appname-$id et $appname-$id.$domain au fichier hosts
ex: 127.0.0.1   $appname-$id.$domain    $appname-$id"
  echo "l'instalation de $appname peut prendre 5 à 6 minutes"
}

grafana() {
  
  appname=grafana
  check

  clear
  echo
  echo " Entrez un nom de domaine ex : exemple.fr"
  read domain
  
subnet
  echo
  echo "Install $appname-$id"

  echo "1 - Create directories ./$appname-$id/{etc,data}"
  mkdir -p $DIR/$appname-$id/{etc,data}
  chmod 775 -R $DIR/$appname-$id/

  echo "2 - Create docker compose for $appname-$id"

echo "
version: '3.6'
services:
  $appname-$id:
    image: $appname/$appname
    container_name: $appname-$id
    user: 0:0
    ports:
     - $port3000:3000
    volumes:
     - $appname-$id-data:/var/lib/grafana
     - $appname-$id-etc:/etc/grafana/provisioning/
    labels:
      - traefik.enable=true
      - traefik.http.routers.$appname-$id-frontend.rule=Host(\`$appname-$id.$domain\`)
      - traefik.http.routers.$appname-$id-frontend.entrypoints=secureweb
      - traefik.http.services.$appname-$id-frontend.loadbalancer.server.port=3000
      - traefik.http.routers.$appname-$id-frontend.service=$appname-$id-frontend
      - traefik.http.routers.$appname-$id-frontend.tls.certresolver=cert
    networks:
     - frontend
volumes:
  $appname-$id-etc:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: ./$appname-$id/etc
  $appname-$id-data:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: ./$appname-$id/data
networks:
  frontend:
    external:
      name: $name_subnet
" > $DIR/$appname-$id/docker-compose.yml

  echo "3 - Run $appname-$id "
  docker-compose -f $DIR/$appname-$id/docker-compose.yml up -d
  sleep 10s

  echo "4 - Auto setup for local prometheus"
  user=admin
  password=admin
  url="\"http://localhost:$port3000/api/datasources\""
  portProm=9091
  post="'Content-Type: application/json;charset=UTF-8'" 
  data="'{\"name\":\"test\",\"type\":\"prometheus\",\"access\":\"proxy\",\"url\":\"http://localhost:$portProm\",\"basicAuth\":false,\"isDefault\":true}'"
  clear
  echo
  read -p "Avez vous un conteneur prometheus [O/N] ? : " response
  if [[ "$response" ==  [oO] ]]; then
   clear
   echo "voulez vous utiliser les parametres par défaut [O/N] ?"
   echo
   echo "user = $user"
   echo "password = $password"
   echo "port_prometheus = $portProm"
   read response
   if [[ "$response" ==  [oO] ]]; then
   echo "OK"
   curl --user $user:$password $url -X POST -H $post --data-binary $data
   echo "curl --user $user:$password $url -X POST -H $post --data-binary $data"
   else
   read -p "quel est le user de $appname : " user
   read -p "quel est le password de $appname : " password
   read -p "quel est le port de prometheus : " portProm
   data="'{\"name\":\"test\",\"type\":\"prometheus\",\"access\":\"proxy\",\"url\":\"http://localhost:$portProm\",\"basicAuth\":false,\"isDefault\":true}'"
   curl --user $user:$password $url -X POST -H $post --data-binary $data
   fi
  else
   echo
   echo "Si vous voulez uttilisez grafana avec prometheus il faudra utiliser la commande suivante"
   echo "curl --user $user:$password $url -X POST -H $post --data-binary $data"
   echo "voir fichier $DIR/$appname-$id/commande_pour_prometheus"
   echo
  fi
   echo "N'oubliez pas de changer les valeurs en fonction de votre conteneur $appname et prometheus
   $appname => user = $user
   $appname => password = $password
   $appname => port = $port3000
   $appname => url = $url
   prometheus => port = $portProm
   prometheus => data = {
                          \"name\":\"$nameProm\",
                          \"isDefault\":true,
                          \"type\":\"test\",
                          \"url\":\"http://localhost:$portProm\",
                          \"access\":\"proxy\",
                          \"basicAuth\":false
                        }
   curl --user $user:$password $url -X POST -H $post --data-binary $data" > $DIR/$appname-$id/commande_pour_prometheus

  ip
}

ip() {
  for i in $(docker ps -q); do
    (( docker inspect $i -f "{{.HostConfig.NetworkMode}}//{{range .NetworkSettings.Networks}}{{.IPAddress}} - {{end}}{{.Name}} -" ; docker port $i ) | paste -d" " -s ) ;done
}

init() {
  docker ps -q | xargs docker stop
  docker ps -aq | xargs docker rm
  docker network ls -q | xargs docker network rm
  docker volume ls -q | xargs docker volume rm
  docker images -q | xargs docker rmi
}

jenkins() {
  
  appname=jenkins
  check

  clear
  echo
  echo " Entrez un nom de domaine ex : exemple.fr"
  read domain
  
  echo
  echo "Install $appname-$id"

  echo "1 - Create directories ./$appname-$id/"
  mkdir -p $DIR/$appname-$id/data
  sudo chmod 700 $DIR/$appname-$id/data

  echo "2 - Create docker-compose file"
echo "
version: '3.6'
services:
  $appname-$id:
    image: jenkins4eval/jenkins
    container_name: $appname-$id
    user: 0:0
    ports:
      - $port8080:8080
      - $port443:443
      - $port5000:5000
    volumes:
      - $appname-$id-data:/var/jenkins_home:z
    labels:
      - traefik.enable=true
      - traefik.http.routers.$appname-$id-frontend.rule=Host(\`$appname-$id.$domain\`)
      - traefik.http.routers.$appname-$id-frontend.entrypoints=secureweb
      - traefik.http.services.$appname-$id-frontend.loadbalancer.server.port=8080
      - traefik.http.routers.$appname-$id-frontend.service=$appname-$id-frontend
      - traefik.http.routers.$appname-$id-frontend.tls.certresolver=cert
    networks:
      - frontend
volumes:
  $appname-$id-data:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: ./$appname-$id/data
networks:
  frontend:
    external:
      name: $name_subnet
" >$DIR/$appname-$id/docker-compose.yml

  echo "3 - Run $appname-$id "
  docker-compose -f $DIR/$appname-$id/docker-compose.yml up -d
  sleep 5s
  clear
  echo
  echo "Traitement en cours..."
  sleep 10s
  clear
  echo
  echo "Veuillez patienter..."
  sleep 10s
  clear
  echo
  echo "Génération du mot de passe..."
  sleep 10s
  clear
  echo
  echo "mot de passe initital:"
  echo
  sudo cat $DIR/$appname-$id/data/secrets/initialAdminPassword
  echo
  ip
}

joomla() {
  
  appname=joomla
  check
  port80Bis=$(($port80 + 1))

  clear
  echo
  echo " Entrez un nom de domaine ex : exemple.fr"
  # read domain

  echo
  echo "Install $appname-$id"

  echo "1 - Create directories ./$appname-$id/"
  mkdir -p $DIR/$appname-$id/{data,db}

  echo "2 - Create config file $appname-$id.yml "
echo "
version: '3.6'

services:
  $appname-$id:
    image: joomla
    container_name: $appname-$id
    restart: always
    links:
      - $appname-$id-db:mysql
    ports:
      - $port80:80
    environment:
      JOOMLA_DB_HOST: $appname-$id-db
      JOOMLA_DB_PASSWORD: $appname
    labels:
      - traefik.enable=true
      - traefik.http.routers.$appname-$id-frontend.rule=Host(\`$appname-$id.$domain\`)
      - traefik.http.routers.$appname-$id-frontend.entrypoints=secureweb
      - traefik.http.services.$appname-$id-frontend.loadbalancer.server.port=80
      - traefik.http.routers.$appname-$id-frontend.service=$appname-$id-frontend
      - traefik.http.routers.$appname-$id-frontend.tls.certresolver=cert
    networks:
      - $appname-$id
      - frontend

  $appname-$id-db:
    image: mysql:5.6
    container_name: $appname-$id-db
    restart: always
    environment:
      MYSQL_ROOT_PASSWORD: $appname
    networks:
      - $appname-$id

  $appname-$id-myadmin:
    depends_on:
      - $appname-$id-db
    image: phpmyadmin
    container_name: $appname-$id-myadmin
    restart: always
    ports:
      - $port80Bis:80
    environment:
      PMA_HOST: $appname-$id-db
      MYSQL_ROOT_PASSWORD: $appname
    labels:
      - traefik.enable=true
      - traefik.http.routers.$appname-$id-myadmin-frontend.rule=Host(\`$appname-$id-myadmin.$domain\`)
      - traefik.http.routers.$appname-$id-myadmin-frontend.entrypoints=secureweb
      - traefik.http.services.$appname-$id-myadmin-frontend.loadbalancer.server.port=80
      - traefik.http.routers.$appname-$id-myadmin-frontend.service=$appname-$id-frontend
      - traefik.http.routers.$appname-$id-myadmin-frontend.tls.certresolver=cert
    networks:
      - $appname-$id
      - frontend

networks:
  $appname-$id:
    name: $appname-$id
  frontend:
    external:
      name: $name_subnet
" > $DIR/$appname-$id/docker-compose.yml

  echo "3 - Run $appname-$id "
  docker-compose -f $DIR/$appname-$id/docker-compose.yml up -d

  ip
}

kafka() {
  echo
  echo "Install Kafka"

  echo "1 - Create directories ./kafka/"
  mkdir -p $DIR/kafka/

  echo " 
version: '3.6'
services:
  zookeeper-1:
    image: confluentinc/cp-zookeeper:latest
    restart: always
    environment:
      ZOOKEEPER_SERVER_ID: 1
      ZOOKEEPER_CLIENT_PORT: 22181
      ZOOKEEPER_TICK_TIME: 2000
      ZOOKEEPER_INIT_LIMIT: 5
      ZOOKEEPER_SYNC_LIMIT: 2
      ZOOKEEPER_SERVERS: localhost:22888:23888;localhost:32888:33888;localhost:42888:43888
    network_mode: host

  zookeeper-2:
    image: confluentinc/cp-zookeeper:latest
    restart: always
    environment:
      ZOOKEEPER_SERVER_ID: 2
      ZOOKEEPER_CLIENT_PORT: 32181
      ZOOKEEPER_TICK_TIME: 2000
      ZOOKEEPER_INIT_LIMIT: 5
      ZOOKEEPER_SYNC_LIMIT: 2
      ZOOKEEPER_SERVERS: localhost:22888:23888;localhost:32888:33888;localhost:42888:43888
    network_mode: host

  zookeeper-3:
    image: confluentinc/cp-zookeeper:latest
    restart: always
    environment:
      ZOOKEEPER_SERVER_ID: 3
      ZOOKEEPER_CLIENT_PORT: 42181
      ZOOKEEPER_TICK_TIME: 2000
      ZOOKEEPER_INIT_LIMIT: 5
      ZOOKEEPER_SYNC_LIMIT: 2
      ZOOKEEPER_SERVERS: localhost:22888:23888;localhost:32888:33888;localhost:42888:43888
    network_mode: host

  kafka-1:
    image: confluentinc/cp-kafka:latest
    restart: always
    network_mode: host
    depends_on:
      - zookeeper-1
      - zookeeper-2
      - zookeeper-3
    environment:
      KAFKA_BROKER_ID: 1
      KAFKA_ZOOKEEPER_CONNECT: localhost:22181,localhost:32181,localhost:42181
      KAFKA_ADVERTISED_LISTENERS: PLAINTEXT://localhost:19092
    ports:
      - 19092:19092
  
  kafka-2:
    image: confluentinc/cp-kafka:latest
    restart: always
    network_mode: host
    depends_on:
      - zookeeper-1
      - zookeeper-2
      - zookeeper-3
    environment:
      KAFKA_BROKER_ID: 2
      KAFKA_ZOOKEEPER_CONNECT: localhost:22181,localhost:32181,localhost:42181
      KAFKA_ADVERTISED_LISTENERS: PLAINTEXT://localhost:29092
    ports:
      - 29092:29092
  
  kafka-3:
    image: confluentinc/cp-kafka:latest
    restart: always
    network_mode: host
    depends_on:
      - zookeeper-1
      - zookeeper-2
      - zookeeper-3
    environment:
      KAFKA_BROKER_ID: 3
      KAFKA_ZOOKEEPER_CONNECT: localhost:22181,localhost:32181,localhost:42181
      KAFKA_ADVERTISED_LISTENERS: PLAINTEXT://localhost:39092
    ports:
      - '39092:39092' 
" >$DIR/$appname-$id/docker-compose.yml

  echo "3 - Run kafka"
  docker-compose -f $DIR/$appname-$id/docker-compose.yml up -d

  echo "4 - Usage"
  echo "
  Usage :
  * create topics : 
      docker run --net=host --rm confluentinc/cp-kafka:latest kafka-topics --create --topic mytopic --partitions 2 --replication-factor 2 --if-not-exists --zookeeper localhost:32181

  * insert data :
      docker run --net=host --rm confluentinc/cp-kafka:latest bash -c 'seq 100 | kafka-console-producer --broker-list localhost:29092 --topic mytopic'

  * request data :
      docker run --net=host --rm confluentinc/cp-kafka:latest kafka-console-consumer --bootstrap-server localhost:29092 --topic mytopic --from-beginning

  "
  ip
}

lamp() {
  
  appname=lamp
  check
  port80Bis=$(($port80 + 1))

  clear
  echo
  echo " Entrez un nom de domaine ex : exemple.fr"
  read domain
  
  echo
  echo "Install $appname-$id"

  echo "1 - Create directories ./$appname-$id/"
  mkdir -p $DIR/$appname-$id/{php,data,conf,logs,$appname-db}

  echo "3 - Create config file $appname-$id.yml "
echo "
FROM php:apache-buster
ARG DEBIAN_FRONTEND=noninteractive
# Update
RUN apt-get -y update --fix-missing && apt-get upgrade -y && rm -rf /var/lib/apt/lists/*
# Utils
RUN apt-get -y update && apt-get -y --no-install-recommends install apt-utils locales nano wget dialog libsqlite3-dev libsqlite3-0 default-mysql-client zlib1g-dev libzip-dev libicu-dev && \
    apt-get -y --no-install-recommends install --fix-missing apt-utils build-essential git curl libonig-dev && \ 
    apt-get -y --no-install-recommends install --fix-missing libcurl4 libcurl4-openssl-dev zip unzip openssl && \
    rm -rf /var/lib/apt/lists/* && \
    curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
# locales
RUN echo \"en_US.UTF-8 UTF-8\" > /etc/locale.gen && \
    echo \"fr_FR.UTF-8 UTF-8\" >> /etc/locale.gen && \
    locale-gen
# Install xdebug
RUN pecl install xdebug-2.8.0 && \
    docker-php-ext-enable xdebug
# Other PHP7 Extensions
RUN docker-php-ext-install pdo_mysql pdo_sqlite mysqli curl tokenizer json zip -j$(nproc) intl mbstring gettext
# Enable apache modules
RUN a2enmod rewrite headers
# Cleanup
RUN rm -rf /usr/src/*
WORKDIR /var/www/html
RUN echo \" <?php phpinfo(); ?> \" > index.php
"> $DIR/$appname-$id/Dockerfile

  echo "3 - Create config file $appname-$id.yml "
  echo "
version: '3.6'
services:
  $appname-$id-db:
    container_name: $appname-$id-db
    image: mariadb/server:latest
    restart: always
    volumes:
      - $appname-$id-db:/var/lib/mysql/
    ports:
      - $port3306:3306
    environment:
      MYSQL_ROOT_PASSWORD: password
      MYSQL_DATABASE: $appname-$id
      MYSQL_USER: $appname
      MYSQL_PASSWORD: $appname
    networks:
      - $appname-$id
  $appname-$id-apache_php:
    depends_on:
      - $appname-$id-db
    container_name: $appname-$id-apache_php
    build:
      context: .
    restart: always
    ports:
      - $port80:80
      - $port443:443
    volumes:
      - $appname-$id-data:/var/www/html
      - $appname-$id-php:/usr/local/etc/php
      - $appname-$id-conf:/etc/apache2
      - $appname-$id-logs:/var/log/apache2
    labels:
      - traefik.enable=true
      - traefik.http.routers.$appname-$id-frontend.rule=Host(\`$appname-$id.$domain\`)
      - traefik.http.routers.$appname-$id-frontend.entrypoints=secureweb
      - traefik.http.services.$appname-$id-frontend.loadbalancer.server.port=80
      - traefik.http.routers.$appname-$id-frontend.service=$appname-$id-frontend
      - traefik.http.routers.$appname-$id-frontend.tls.certresolver=cert
    networks:
      - $appname-$id
      - frontend
  $appname-$id-myadmin:
    depends_on:
      - $appname-$id-db
    container_name: $appname-$id-myadmin
    image: phpmyadmin
    restart: always
    ports:
      - $port80Bis:80
    environment:
      PMA_HOST: $appname-db
      MYSQL_ROOT_PASSWORD: password 
    labels:
      - traefik.enable=true
      - traefik.http.routers.$appname-$id-myadmin-frontend.rule=Host(\`$appname-$id-myadmin.$domain\`)
      - traefik.http.routers.$appname-$id-myadmin-frontend.entrypoints=secureweb
      - traefik.http.services.$appname-$id-myadmin-frontend.loadbalancer.server.port=80
      - traefik.http.routers.$appname-$id-myadmin-frontend.service=$appname-$id-frontend
      - traefik.http.routers.$appname-$id-myadmin-frontend.tls.certresolver=cert
    networks:
      - $appname-$id
      - frontend
volumes:
  $appname-$id-db:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: ./$appname-$id/$appname-db
  $appname-$id-data:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: ./$appname-$id/data
  $appname-$id-php:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: ./$appname-$id/php
  $appname-$id-conf:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: ./$appname-$id/conf
  $appname-$id-logs:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: ./$appname-$id/logs
networks:
  $appname-$id:
    name: $appname-$id
  frontend:
    external:
      name: $name_subnet
  " > $DIR/$appname-$id/docker-compose.yml

echo "3 - Run $appname-$id"
  docker-compose -f $DIR/$appname-$id/docker-compose.yml up -d

ip
}

lemp() {
  
  appname=lemp
  check

  clear
  echo
  echo " Entrez un nom de domaine ex : exemple.fr"
  read domain
 
  
  port80Bis=$(($port80 + 1))

  echo "Install $appname-$id "

  echo "1 - Create directories ./$appname-$id/"
  mkdir -p $DIR/$appname-$id/{data,conf,db}
  chmod 775 -R $DIR/$appname-$id/

  echo "<?php phpinfo(); ?>" > $DIR/$appname-$id/data/index.php

echo "
server {
    index index.php index.html;
    server_name $appname-$id.localhost;
    error_log  /var/log/nginx/error.log;
    access_log /var/log/nginx/access.log;
    root /usr/share/nginx/html;

    location ~ \.php$ {
        try_files \$uri =404;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass $appname-$id-php:$port9000;
        fastcgi_index index.php;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
        fastcgi_param PATH_INFO \$fastcgi_path_info;
    }
}
" > $DIR/$appname-$id/conf/default.conf

  echo "2 - Create config file lemp.yml "
  echo "
version: '3.6'
services:
  $appname-$id-db:
    container_name: $appname-$id-db
    image: mariadb/server:latest
    restart: always
    volumes:
      - $appname-$id-db:/var/lib/mysql/
    ports:
      - $port3306:3306
    environment:
      MYSQL_ROOT_PASSWORD: rootpassword
      MYSQL_DATABASE: $appname-$id
      MYSQL_USER: $appname
      MYSQL_PASSWORD: $appname
    networks:
      - $appname-$id
  $appname-$id-nginx:
    depends_on:
      - $appname-$id-db
    container_name: $appname-$id-nginx
    image: nginx:latest
    ports:
      - $port80:80
    volumes:
      - $appname-$id-data:/usr/share/nginx/html
      - $appname-$id-conf:/etc/nginx/conf.d
    labels:
      - traefik.enable=true
      - traefik.http.routers.$appname-$id-frontend.rule=Host(\`$appname-$id.$domain\`)
      - traefik.http.routers.$appname-$id-frontend.entrypoints=secureweb
      - traefik.http.services.$appname-$id-frontend.loadbalancer.server.port=80
      - traefik.http.routers.$appname-$id-frontend.service=$appname-$id-frontend
      - traefik.http.routers.$appname-$id-frontend.tls.certresolver=cert
    networks:
      - $appname-$id
      - frontend
  $appname-$id-php:
    container_name: $appname-$id-php
    image: php:fpm
    ports:
      - $port9000:9000
    volumes:
      - $appname-$id-data:/usr/share/nginx/html
    networks:
      - $appname-$id
  $appname-$id-myadmin:
    depends_on:
      - $appname-$id-db
    container_name: $appname-$id-myadmin
    image: phpmyadmin
    restart: always
    ports:
      - $port80Bis:80
    environment:
      PMA_HOST: $appname-$id-db
      MYSQL_ROOT_PASSWORD: rootpassword 
    labels:
      - traefik.enable=true
      - traefik.http.routers.$appname-$id-myadmin-frontend.rule=Host(\`$appname-$id-myadmin.$domain\`)
      - traefik.http.routers.$appname-$id-myadmin-frontend.entrypoints=secureweb
      - traefik.http.services.$appname-$id-myadmin-frontend.loadbalancer.server.port=80
      - traefik.http.routers.$appname-$id-myadmin-frontend.service=$appname-$id-frontend
      - traefik.http.routers.$appname-$id-myadmin-frontend.tls.certresolver=cert
    networks:
      - $appname-$id
      - frontend
volumes:
  $appname-$id-db:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: ./$appname-$id/db
  $appname-$id-data:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: ./$appname-$id/data
  $appname-$id-conf:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: ./$appname-$id/conf
networks:
  $appname-$id:
    name: $appname-$id
  frontend:
    external:
      name: $name_subnet
  " > $DIR/$appname-$id/docker-compose.yml
  
  echo "3 - Run $appname-$id"
  docker-compose -f $DIR/$appname-$id/docker-compose.yml up -d

  ip
}

mariadb() {
 
 appname=mariadb
 check
 
  echo
  echo "Install $appname-$id"

  echo "1 - Create directories ./$appname-$id/data"
  mkdir -p $DIR/$appname-$id/data

  echo "2 - Create docker-compose file"
echo "
version: '3.6'
services:
  $appname-$id:
    container_name: $appname-$id
    image: $appname
    volumes:
     - $appname-$id-data:/var/lib/mysql/
    environment:
      MYSQL_ROOT_PASSWORD: rootpassword
      MYSQL_DATABASE: $appname-$id
      MYSQL_USER: $appname
      MYSQL_PASSWORD: $appname
    ports:
      - $port3306:3306
    networks:
      - frontend
volumes:
  $appname-$id-data:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: ./$appname-$id/data
networks:
  frontend:
    external:
      name: $name_subnet
" >$DIR/$appname-$id/docker-compose.yml

  echo "3 - Run $appname-$id"
  docker-compose -f $DIR/$appname-$id/docker-compose.yml up -d

  echo "4 - Credentials"
  echo "
        MYSQL_ROOT_PASSWORD: rootpassword
        MYSQL_DATABASE: $appname-$id
        MYSQL_USER: $appname
        MYSQL_PASSWORD: $appname
        
        Pour uttiliser avec la commande mysql installer mariadb sur votre machine 'sudo apt install mariadb-server'
        ex : mysql -h 172.20.0.2 -u $appname -p
        Sinon connecter vous au conteneur : sudo docker exec -it mariadb-1 /bin/bash
        puis faire : mysql -u $appname -p
        exit 2x pour sortir
  "
  ip
}

mongodb() {

 appname=mongo
 check

   clear
  echo
  echo " Entrez un nom de domaine ex : exemple.fr"
  read domain
 
  echo
  echo "Install $appname-$id"

  echo "1 - Create directories ./$appname-$id/data"
  mkdir -p $DIR/$appname-$id/data

  echo "2 - Create docker-compose"
echo "
version: '3.6'
services:
  $appname-$id:
    container_name: $appname-$id
    image: $appname:latest
    restart: always
    volumes:
     - $appname-$id-data:/data/db
    environment:
      MONGO_INITDB_DATABASE: $appname-db
      MONGO_INITDB_ROOT_USERNAME: $appname
      MONGO_INITDB_ROOT_PASSWORD: $appname
    ports:
      - $port3306:27017
    networks:
      - $appname-$id
  $appname-$id-express:
    container_name: $appname-$id-express
    image: $appname-express
    restart: always
    ports:
      - $port8080:8081
    environment:
      ME_CONFIG_MONGODB_ADMINUSERNAME: $appname
      ME_CONFIG_MONGODB_AUTH_DATABASE: $appname-db
      ME_CONFIG_MONGODB_ADMINPASSWORD: $appname
      ME_CONFIG_MONGODB_SERVER: $appname-$id
    labels:
      - traefik.enable=true
      - traefik.http.routers.$appname-$id-frontend.rule=Host(\`$appname-$id.$domain\`)
      - traefik.http.routers.$appname-$id-frontend.entrypoints=secureweb
      - traefik.http.services.$appname-$id-frontend.loadbalancer.server.port=8081
      - traefik.http.routers.$appname-$id-frontend.service=$appname-$id-frontend
      - traefik.http.routers.$appname-$id-frontend.tls.certresolver=cert
    networks:
      - $appname-$id
      - frontend
volumes:
  $appname-$id-data:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: ./$appname-$id/data
networks:
  $appname-$id:
    name: $appname-$id
  frontend:
    external:
      name: $name_subnet
" >$DIR/$appname-$id/docker-compose.yml

  echo "3 - Run $appname-$id"
  docker-compose -f $DIR/$appname-$id/docker-compose.yml up -d

  ip
}

mysql() {
 
 appname=mysql
 check
 
  echo
  echo "Install $appname-$id"

  echo "1 - Create directories ./$appname-$id/data"
  mkdir -p $DIR/$appname-$id/data

  echo "2 - Create docker-compose file"
  echo "
version: '3.6'
services:
  $appname-$id:
    container_name: $appname-$id
    image: $appname/$appname-server
    restart: always
    volumes:
      - $appname-$id-data:/var/lib/mysql/
    environment:
      MYSQL_ROOT_PASSWORD: rootpassword
      MYSQL_DATABASE: $appname-$id
      MYSQL_USER: $appname
      MYSQL_PASSWORD: $appname
    ports:
      - $port3306:3306
    networks:
      - frontend
volumes:
  $appname-$id-data:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: ./$appname-$id/data
networks:
  frontend:
    external:
      name: $name_subnet
    " > $DIR/$appname-$id/docker-compose.yml

  echo "3 - Run $appname-$id"
  docker-compose -f $DIR/$appname-$id/docker-compose.yml up -d
  
  echo "4 - Credentials"
  echo "
        MYSQL_ROOT_PASSWORD: rootpassword
        MYSQL_DATABASE: $appname-$id
        MYSQL_USER: $appname
        MYSQL_PASSWORD: $appname

        Pour uttiliser avec la commande mysql installer mysql sur votre machine 'sudo apt install mysql-server'
        ex : mysql -h 172.20.0.2 -u $appname -p
        Sinon connecter vous au conteneur avec : sudo docker exec -it mariadb-1 /bin/bash
        puis faire : mysql -u $appname -p
        exit 2x pour sortir
  "
  ip
}

nginx() {
 
 appname=nginx
 check

  clear
  echo
  echo " Entrez un nom de domaine ex : exemple.fr"
  read domain
 
  echo
  echo "Install $appname-$id"

  echo "1 - Create directories ./$appname-$id/{config,data}"
  mkdir -p $DIR/$appname-$id/{config,data}

  echo "Hello World" > $DIR/$appname-$id/data/index.html

echo "3 - Create file default.conf"
echo "
server {
    index index.html;
    server_name localhost;
    error_log  /var/log/nginx/error.log;
    access_log /var/log/nginx/access.log;
    root /usr/share/nginx/html;
}
" >$DIR/$appname-$id/config/default.conf

  echo "2 - Create config file $appname-$id.yml "
  echo "
version: '3.6'
services:
  $appname-$id:
    container_name: $appname-$id
    image: $appname:latest
    ports:
      - $port80:80
      - $port443:443
    volumes:
      - $appname-$id-data:/usr/share/nginx/html
      - $appname-$id-config:/etc/nginx/conf.d
    labels:
      - traefik.enable=true
      - traefik.http.routers.$appname-$id-frontend.rule=Host(\`$appname-$id.$domain\`)
      - traefik.http.routers.$appname-$id-frontend.entrypoints=secureweb
      - traefik.http.services.$appname-$id-frontend.loadbalancer.server.port=80
      - traefik.http.routers.$appname-$id-frontend.service=$appname-$id-frontend
      - traefik.http.routers.$appname-$id-frontend.tls.certresolver=cert
    networks:
    - frontend
volumes:
  $appname-$id-data:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: ./$appname-$id/data
  $appname-$id-config:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: ./$appname-$id/config
networks:
  frontend:
    external:
      name: $name_subnet
  " > $DIR/$appname-$id/docker-compose.yml
  
  echo "3 - Run $appname-$id"
  docker-compose -f $DIR/$appname-$id/docker-compose.yml up -d

  ip
}

phpmyadmin() {
 
 appname=phpmyadmin
 check

  clear
  echo
  echo " Entrez un nom de domaine ex : exemple.fr"
  read domain

  echo " Entrez le nom du container de la base de données a lier ex : mysql-1 ou mariadb-1"
  read host
 
  echo
  echo "Install $appname-$id"

  echo "1 - Create directories ./$appname-$id/"
  mkdir -p $DIR/$appname-$id/

  echo "2 - Create config file $appname-$id.yml "
  echo "
version: '3.6'
services:
  $appname-$id:
    container_name: $appname-$id
    image: $appname
    restart: always
    ports:
      - $port80:80
    environment:
      PMA_HOST: $host
      MYSQL_ROOT_PASSWORD: password
    labels:
      - traefik.enable=true
      - traefik.http.routers.$appname-$id-frontend.rule=Host(\`$appname-$id.$domain\`)
      - traefik.http.routers.$appname-$id-frontend.entrypoints=secureweb
      - traefik.http.services.$appname-$id-frontend.loadbalancer.server.port=80
      - traefik.http.routers.$appname-$id-frontend.service=$appname-$id-frontend
      - traefik.http.routers.$appname-$id-frontend.tls.certresolver=cert
    networks:
    - frontend  
networks:
  frontend:
    external:
      name: $name_subnet
  " > $DIR/$appname-$id/docker-compose.yml

  echo "3 - Run $appname-$id"
  docker-compose -f $DIR/$appname-$id/docker-compose.yml up -d
  ip
}

portainer() {
  
  appname=portainer
  check

  clear
  echo
  echo " Entrez un nom de domaine ex : exemple.fr"
  read domain

  echo
  echo "Install $appname-$id"

  echo "1 - Create directories ./$appname-$id/data"
  mkdir -p $DIR/$appname-$id/data

echo "2 - Create docker-compose file"
echo "
version: '3.6'
services:
  $appname-$id:
    container_name: $appname-$id
    image: $appname/$appname-ce:2.0.0
    command: -H unix:///var/run/docker.sock
    restart: always
    ports:
      - $port9000:9000
    networks:
      - frontend
    labels:
      - traefik.enable=true
      - traefik.http.routers.$appname-$id-frontend.rule=Host(\`$appname-$id.$domain\`)
      - traefik.http.routers.$appname-$id-frontend.entrypoints=secureweb
      - traefik.http.services.$appname-$id-frontend.loadbalancer.server.port=9000
      - traefik.http.routers.$appname-$id-frontend.service=$appname-$id-frontend
      - traefik.http.routers.$appname-$id-frontend.tls.certresolver=cert
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - $appname-$id-data:/data
volumes:
  $appname-$id-data:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: ./$appname-$id/data
networks:
  frontend:
    external:
      name: $name_subnet

" >$DIR/$appname-$id/docker-compose.yml

  echo "3 - Run $appname-$id"
  docker-compose -f $DIR/$appname-$id/docker-compose.yml up -d

  echo " To use $appname select local and click connect"
  ip
}

postgres() {
  
  appname=postgres
  check
  
  echo
  echo "Install $appname-$id"

  echo "1 - Create directories ./$appname-$id/data"
  mkdir -p $DIR/$appname-$id/data

  echo "2 - Create docker-compose file"
  echo "
version: '3.6'
services:
  $appname-$id:
    image: $appname
    container_name: $appname-$id
    environment:
      - POSTGRES_USER=$appname
      - POSTGRES_PASSWORD=$appname
      - POSTGRES_DB=$appname-$id
    ports:
      - $port5432:5432
    volumes:
      - $appname-$id-data:/var/lib/postgresql/data/
    networks:
      - frontend     
volumes:
  $appname-$id-data:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: ./$appname-$id/data
networks:
  frontend:
    external:
      name: $name_subnet
" >$DIR/$appname-$id/docker-compose.yml

echo "3 - Run $appname-$id"
docker-compose -f $DIR/$appname-$id/docker-compose.yml up -d
  echo "
  Credentials:
                  user: $appname
                  password: $appname
                  db: $appname-$id
                  port: $port5432


  
  command : 

  Pour uttiliser avec la commande psql installer mariadb sur votre machine 'sudo apt install postgresql'
  ex : psql -h  172.28.0.4 -U $appname $appname-$id
  Sinon connecter vous au conteneur : sudo docker exec -it $appname-$id /bin/bash
  puis faire : psql -U $appname
  exit 2x pour sortir
  "
  ip
}

prometheus() {
 
 appname=prometheus
 check

 clear
  echo
  echo " Entrez un nom de domaine ex : exemple.fr"
  read domain
 
  echo
  echo "Install $appname-$id"

  echo "1 - Create directories ./$appname-$id/{etc,data}"
  mkdir -p $DIR/$appname-$id/{etc,data}

  echo "2 - Create config file $appname-$id.yml "
  echo "
global:
  scrape_interval:     5s # By default, scrape targets every 15 seconds.
  evaluation_interval: 5s # By default, scrape targets every 15 seconds.
  # scrape_timeout is set to the global default (10s).

  # Attach these labels to any time series or alerts when communicating with
  # external systems (federation, remote storage, Alertmanager).
  external_labels:
    monitor: 'codelab-monitor'

# Load rules once and periodically evaluate them according to the global 'evaluation_interval'.
rule_files:
  # - \"first.rules\"
  # - \"second.rules\"

# A scrape configuration containing exactly one endpoint to scrape:
# Here it's Prometheus itself.
scrape_configs:
- job_name: 'node'
  static_configs:
  - targets: ['traefik:8080']
" > $DIR/$appname-$id/etc/prometheus.yml
chmod 775 -R $DIR/$appname-$id/

echo "3 - Create docker compose for $appname-$id "
echo "
version: '3.6'
services:
  $appname-$id:
    image: prom/prometheus
    container_name: $appname-$id
    volumes:
     - $appname-$id-etc:/etc/prometheus/
     - $appname-$id-data:/prometheus/
    command: \"--config.file=/etc/prometheus/prometheus.yml --storage.tsdb.path=/prometheus\"
    ports:
     - $port9091:9090
    labels:
      - traefik.enable=true
      - traefik.http.routers.$appname-$id-frontend.rule=Host(\`$appname-$id.$domain\`)
      - traefik.http.routers.$appname-$id-frontend.entrypoints=secureweb
      - traefik.http.services.$appname-$id-frontend.loadbalancer.server.port=9090
      - traefik.http.routers.$appname-$id-frontend.service=$appname-$id-frontend
      - traefik.http.routers.$appname-$id-frontend.tls.certresolver=cert
    networks:
     - frontend
volumes:
  $appname-$id-etc:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: ./$appname-$id/etc/
  $appname-$id-data:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: ./$appname-$id/data/
networks:
  frontend:
    external:
      name: $name_subnet
" > $DIR/$appname-$id/docker-compose.yml

  echo "4 - Run $appname-$id "
  docker-compose -f $DIR/$appname-$id/docker-compose.yml up -d

  ip
}

remove-containers() {
docker ps -aq | xargs docker rm -f
}
remove-images() {
docker images -q | xargs docker rmi -f
}
remove-networks() {
docker network ls -q | xargs docker network rm
}
remove-volumes() {
docker volume ls -q | xargs docker volume rm
}
stop() {
docker ps -q | xargs docker stop
}

symfony() {
  
  appname=symfony
  check
  port80Bis=$(($port80 + 1))

  clear
  echo
  echo " Entrez un nom de domaine ex : exemple.fr"
  read domain
  
  echo "Install $appname-$id"

  echo "1 - Create directories ./$appname-$id/"
  mkdir -p $DIR/$appname-$id/{php,data,conf,logs,symfonydb}

  echo "3 - Create config file $appname-$id.yml "
echo "
FROM php:apache-buster
ARG DEBIAN_FRONTEND=noninteractive
# Update
RUN apt-get -y update --fix-missing && apt-get upgrade -y && rm -rf /var/lib/apt/lists/*
# Utils
RUN apt-get -y update && apt-get -y --no-install-recommends install apt-utils locales nano wget dialog libsqlite3-dev libsqlite3-0 default-mysql-client zlib1g-dev libzip-dev libicu-dev && \
    apt-get -y --no-install-recommends install --fix-missing apt-utils build-essential git curl libonig-dev && \ 
    apt-get -y --no-install-recommends install --fix-missing libcurl4 libcurl4-openssl-dev zip unzip openssl && \
    rm -rf /var/lib/apt/lists/* && \
    curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
# locales
RUN echo \"en_US.UTF-8 UTF-8\" > /etc/locale.gen && \
    echo \"fr_FR.UTF-8 UTF-8\" >> /etc/locale.gen && \
    locale-gen
# Install xdebug
RUN pecl install xdebug-2.8.0 && \
    docker-php-ext-enable xdebug
# Other PHP7 Extensions
RUN docker-php-ext-install pdo_mysql pdo_sqlite mysqli curl tokenizer json zip -j$(nproc) intl mbstring gettext
# Enable apache modules
RUN a2enmod rewrite headers
# Cleanup
RUN rm -rf /usr/src/*
WORKDIR /var/www/html
RUN composer create-project symfony/skeleton symfony
WORKDIR /var/www/html/symfony
RUN composer config extra.symfony.allow-contrib true && composer require symfony/orm-pack annotations symfony/form twig security symfony/translation symfony/asset symfony/apache-pack && \
composer require --dev symfony/maker-bundle symfony/var-dumper symfony/profiler-pack && chmod -R 777 /var/www/html/ && sed -i 's/\/var\/www\/html/\/var\/www\/html\/symfony\/public/g' /etc/apache2/sites-available/000-default.conf
  "> $DIR/$appname-$id/Dockerfile

  echo "3 - Create config file $appname-$id.yml "
  echo "
version: '3.6'
services:
  $appname-$id-db:
    container_name: $appname-$id-db
    image: mariadb/server:latest
    restart: always
    volumes:
      - $appname-$id-db:/var/lib/mysql/
    ports:
      - $port3306:3306
    environment:
      MYSQL_ROOT_PASSWORD: password
      MYSQL_DATABASE: $appname-db
      MYSQL_USER: $appname
      MYSQL_PASSWORD: $appname
    networks:
      - $appname-$id
  $appname-$id-apache-php:
    depends_on:
      - $appname-$id-db
    container_name: $appname-$id-apache-php
    build:
      context: .
    restart: always
    ports:
      - $port80:80
      - $port443:443
    labels:
      - traefik.enable=true
      - traefik.http.routers.$appname-$id-frontend.rule=Host(\`$appname-$id.$domain\`)
      - traefik.http.routers.$appname-$id-frontend.entrypoints=secureweb
      - traefik.http.services.$appname-$id-frontend.loadbalancer.server.port=80
      - traefik.http.routers.$appname-$id-frontend.service=$appname-$id-frontend
      - traefik.http.routers.$appname-$id-frontend.tls.certresolver=cert
    volumes:
      - $appname-$id-data:/var/www/html
      - $appname-$id-php:/usr/local/etc/php
      - $appname-$id-conf:/etc/apache2
    networks:
      - $appname-$id
      - frontend
  $appname-$id-myadmin:
    depends_on:
      - $appname-$id-db
    container_name: $appname-$id-myadmin
    image: phpmyadmin
    restart: always
    ports:
      - $port80Bis:80
    environment:
      PMA_HOST: $appname-$id-db
      MYSQL_ROOT_PASSWORD: password 
    labels:
      - traefik.enable=true
      - traefik.http.routers.$appname-$id-myadmin-frontend.rule=Host(\`$appname-$id-myadmin.$domain\`)
      - traefik.http.routers.$appname-$id-myadmin-frontend.entrypoints=secureweb
      - traefik.http.services.$appname-$id-myadmin-frontend.loadbalancer.server.port=80
      - traefik.http.routers.$appname-$id-myadmin-frontend.service=$appname-$id-frontend
      - traefik.http.routers.$appname-$id-myadmin-frontend.tls.certresolver=cert
    networks:
      - $appname-$id
      - frontend
volumes:
  $appname-$id-db:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: ./$appname-$id/symfonydb
  $appname-$id-data:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: ./$appname-$id/data
  $appname-$id-php:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: ./$appname-$id/php
  $appname-$id-conf:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: ./$appname-$id/conf
  $appname-$id-logs:
    driver_opts:
      o: bind
      type: none
      device: ./$appname-$id/logs
networks:
  $appname-$id:
    name: $appname-$id
  frontend:
    external:
      name: $name_subnet
  " > $DIR/$appname-$id/docker-compose.yml

echo "3 - Run $appname-$id"
  docker-compose -f $DIR/$appname-$id/docker-compose.yml up -d

  clear
  echo
  echo "L'instalation peut prendre plus ou moins 5 minutes"
  echo
  ip
}

traefik() {
  
  appname=traefik
  check

  clear
  echo
  echo " Entrez un nom de domaine ex : exemple.fr"
  read domain


  echo
  echo "Install $appname-$id"

  echo "1 - Creation des dossiers ./$appname-$id/config/dynamique"
  mkdir -p $DIR/$appname-$id/config/dynamique

  echo "2 création du fichier acme.json"
  touch $DIR/$appname-$id/config/acme.json
  chmod 600 $DIR/$appname-$id/config/acme.json

  echo "3 création  .ovh-api.env"
  echo "
  # OVH API credentials used by Certbot
  OVH_ENDPOINT=ovh-eu
  OVH_APPLICATION_KEY=a5XwgNwRbQZDDzmX
  OVH_APPLICATION_SECRET=R3X3dTdml33iCWx4Fy4236tYiCUuLrts
  OVH_CONSUMER_KEY=bRvLct6HeC2lZyn4m4tCw3AxToV7nzIp
  " > $DIR/$appname-$id/config/.ovh-api.env

  echo "4 - Create $appname.yml"
  echo "
entryPoints:
  web:
    address: \":80\"
  secureweb:
    address: \":443\"
  dashboard:
    address: \":8080\"
log:
  # Default: \"ERROR\"
  level: INFO
api:
  dashboard: true
providers:
  docker:
    endpoint: \"unix:///var/run/docker.sock\"
    # Expose containers by default in $appname
    exposedByDefault: true
  # Enable the file provider to define routers / middlewares / services in file
  file:
    directory: \"/etc/dyn_config/\"
    watch: true

certificatesResolvers:
  cert:
    # Enable ACME (Let's Encrypt): automatic SSL.
    acme:
      email: \"webmaster@$domain\"
      storage: \"/acme.json\"

      ## CA server to use.
      ## Par défaut :
      #caServer: \"https://acme-v02.api.letsencrypt.org/directory\"
      ## Environnement de qualification de LE
      caServer: \"https://acme-staging-v02.api.letsencrypt.org/directory\"

      ## Use a HTTP-01 ACME challenge.
      #httpChallenge:
      #  entryPoint: \"web\"

      # Use a DNS-01 ACME challenge rather than HTTP-01 challenge.
      dnsChallenge:
        provider: \"ovh\"
        resolvers:
          - \"1.1.1.1:53\"
          - \"8.8.8.8:53\"
  "> $DIR/$appname-$id/config/$appname.yml

  echo "5 creation dashboard.yml"
  echo "
http:
  routers:
    api:
      rule: \"Host(\`$appname-$id.$domain\`)\"
      service: api@internal
      middlewares:
        - redirection
    api-secure:
      rule: \"Host(\`$appname-$id.$domain\`)\"
      service: api@internal
      tls:
        certResolver: cert
      middlewares:
        - auth
  middlewares:
    auth:
      basicAuth:
        users:
          #Bcrypt encode password admin
          - \"admin:\$2y\$10\$sqgVdrmQxXQgYIpg3BycVuZ2J8sFSHheVnmd728KkE7fznFSlxA0u\"
    redirection:
      redirectScheme:
        scheme: https
        permanent: true
  " > $DIR/$appname-$id/config/dynamique/dashboard.yml

  echo "6 - Create config file $appname-$id.yml "
echo "
version: '3.6'
services:
  $appname-$id:
    image: $appname:v2.3
    container_name: $appname-$id
    restart: always
    command: --web --docker --web.metrics.prometheus --web.metrics.prometheus.buckets=\"0.1,0.3,1.2,5.0\" --docker.domain=docker.localhost --logLevel=DEBUG
    # Ports d'écoute
    ports:
      - $port80:80
      - $port443:443
    # Variables d'environnement
    env_file:
      - ./config/.ovh-api.env
    volumes:
      # Pour que $appname puisse écouter les events Docker
      - /var/run/docker.sock:/var/run/docker.sock:ro
      # Mapping de la configuration statique
      - ./config/$appname.yml:/etc/$appname/$appname.yml:ro
      # Mapping du dossier de conf dynamique
      - ./config/dynamique:/etc/dyn_config/
      # Mapping du fichier de stockage des certificats
      - ./config/acme.json:/acme.json
    environment:
      - TZ=Europe/Paris
    networks:
      - frontend
networks:
  frontend:
    external:
      name: $name_subnet
" > $DIR/$appname-$id/docker-compose.yml
  
  echo "4 - Run $appname-$id "
  docker-compose -f $DIR/$appname-$id/docker-compose.yml up -d
  clear
  echo
  echo "Pour afficher traefik dans votre navigateur vous devez configurer votre fichier hosts"
  echo "Pour Windows : "
  echo "Allez dans le dossier "
  echo "C:\Windows\System32\drivers\etc"
  echo "et ouvrir 'hosts' en tant qu'administrateur"
  echo "Rajouter la ligne : "
  echo "127.0.0.1 $appname-$id.$domain"
  echo "Pour linux :"
  echo "sudo nano /etc/hosts"
  ip
}

strapi() {
  
  appname=strapi
  check

  clear
  echo
  echo " Entrez un nom de domaine ex : exemple.fr"
  read domain


  echo
  echo "Install $appname-$id"

  echo "1 - Création des dossiers ./$appname-$id/{db,data}"
  mkdir -p $DIR/$appname-$id/{db,data}

  echo "2 - Creation du fichier $appname-$id.yml "
echo "
version: '3.6'
services:
  $appname-$id:
    image: $appname/$appname
    container_name: $appname-$id
    environment:
      DATABASE_CLIENT: mongo
      DATABASE_NAME: $appname
      DATABASE_HOST: $appname-$id-db
      DATABASE_PORT: $port27017
      DATABASE_USERNAME: $appname
      DATABASE_PASSWORD: $appname
    links:
      - $appname-$id-db:mongo
    volumes:
      - ./data:/srv/app
    ports:
      - $port1337:1337
    networks:
      - $appname-$id
      - frontend
    labels:
      - traefik.enable=true
      - traefik.http.routers.$appname-$id-frontend.rule=Host(\`$appname-$id.$domain\`)
      - traefik.http.routers.$appname-$id-frontend.entrypoints=secureweb
      - traefik.http.services.$appname-$id-frontend.loadbalancer.server.port=1337
      - traefik.http.routers.$appname-$id-frontend.service=$appname-$id-frontend
      - traefik.http.routers.$appname-$id-frontend.tls.certresolver=cert

  $appname-$id-db:
    image: mongo
    container_name: $appname-$id-db
    environment:
      MONGO_INITDB_ROOT_USERNAME: $appname
      MONGO_INITDB_ROOT_PASSWORD: $appname
    volumes:
      - ./db:/data/db
    ports:
      - $port27017:27017
    networks:
      - $appname-$id

networks:
  $appname-$id:
    name: $appname-$id
  frontend:
    external:
      name: $name_subnet
" > $DIR/$appname-$id/docker-compose.yml
  
  echo "4 - Run $appname-$id "
  docker-compose -f $DIR/$appname-$id/docker-compose.yml up -d

  clear
  ip
  echo
  echo "Le temps d'installation de strapi avant l'affichage dans le navigateur est de 10 minutes"
  echo "vous pouvez regarder les logs en direct avec portainer ou docker-desktop"
  echo
  sleep 550s
}

wordpress() {
  
  appname=wordpress
  check
  port80Bis=$(($port80 + 1))

  clear
  echo
  echo " Entrez un nom de domaine ex : exemple.fr"
  read domain

  echo
  echo "Install $appname-$id"

  echo "1 - Create directories ./$appname-$id/{db,data}"
  mkdir -p $DIR/$appname-$id/{db,data}
  chmod 775 -R $DIR/$appname-$id/

  echo "2 - Create config file $appname-$id.yml "
  echo "
  version: '3.6'
  services:
    $appname-$id-db:
      container_name: $appname-$id-db
      image: mariadb
      restart: always
      volumes:
        - $appname-$id-db:/var/lib/mysql/
      ports:
        - $port3306:3306
      environment:
        MYSQL_ROOT_PASSWORD: password
        MYSQL_DATABASE: $appname-db
        MYSQL_USER: $appname
        MYSQL_PASSWORD: $appname
      networks:
        - $appname-$id
    $appname-$id:
      depends_on:
        - $appname-$id-db
      container_name: $appname-$id
      image: $appname:latest
      restart: always
      ports:
        - $port80:80
      volumes:
        - $appname-$id-data:/var/www/html/
      environment:
        WORDPRESS_DB_HOST: $appname-$id-db
        WORDPRESS_DB_USER: $appname
        WORDPRESS_DB_PASSWORD: $appname
        WORDPRESS_DB_NAME: $appname-db
      networks:
        - $appname-$id
        - frontend
      labels:
        - traefik.enable=true
        - traefik.http.routers.$appname-$id-frontend.rule=Host(\`$appname-$id.$domain\`)
        - traefik.http.routers.$appname-$id-frontend.entrypoints=secureweb
        - traefik.http.services.$appname-$id-frontend.loadbalancer.server.port=80
        - traefik.http.routers.$appname-$id-frontend.service=$appname-$id-frontend
        - traefik.http.routers.$appname-$id-frontend.tls.certresolver=cert
    $appname-$id-myadmin:
      depends_on:
        - $appname-$id-db
      image: phpmyadmin
      container_name: $appname-$id-myadmin
      restart: always
      ports:
        - $port80Bis:80
      environment:
        PMA_HOST: $appname-$id-db
        MYSQL_ROOT_PASSWORD: password 
      labels:
        - traefik.enable=true
        - traefik.http.routers.$appname-$id-myadmin-frontend.rule=Host(\`$appname-$id-myadmin.$domain\`)
        - traefik.http.routers.$appname-$id-myadmin-frontend.entrypoints=secureweb
        - traefik.http.services.$appname-$id-myadmin-frontend.loadbalancer.server.port=80
        - traefik.http.routers.$appname-$id-myadmin-frontend.service=$appname-$id-frontend
        - traefik.http.routers.$appname-$id-myadmin-frontend.tls.certresolver=cert
      networks:
        - $appname-$id
        - frontend
  volumes:
    $appname-$id-db:
      driver_opts:
        o: bind
        type: none
        device: ./$appname-$id/db
    $appname-$id-data:
      driver_opts:
        o: bind
        type: none
        device: ./$appname-$id/data
  networks:
    $appname-$id:
        name: $appname-$id
    frontend:
      external:
        name: $name_subnet
  " > $DIR/$appname-$id/docker-compose.yml

  echo "3 - Run $appname-$id"
  docker-compose -f $DIR/$appname-$id/docker-compose.yml up -d

  ip
  echo "veuillez patientez le temps de l'installation"
}

yunohost() {
    
  appname=yunohost
  check

  clear
  echo
  echo " Entrez un nom de domaine ex : exemple.fr"
  read domain

  echo
  echo "Installation de $appname-$id"

  echo "1 - Création des dossiers ./$appname-$id/{media,hack_scripts,backup,log}"
  mkdir -p $DIR/$appname-$id/{media,hack_scripts,backup,log}

  echo "2 - Création du fichier hostfiles-hack.service"
  echo "[Unit]
After=sshd.service
[Service]
ExecStart=/usr/local/bin/hostfiles-hack.sh
[Install]
WantedBy=default.target
" > $DIR/$appname-$id/hack_scripts/hostfiles-hack.service

  echo "3 Création du fichier hostfiles-hack.sh"
  echo "#!/bin/bash
TAG=\"#HACKED\"
# hack /etc/hosts
grep -q \"^\$TAG\" /etc/hosts
if [ \"\$1\" != \"0\" ]
then
	cp /etc/hosts /etc/hosts_cp
	sed -i \"1i\$TAG\" /etc/hosts_cp
	umount /etc/hosts
	cp /etc/hosts_cp /etc/hosts
fi
# hack /etc/hostname
cp /etc/hostname /etc/hostname_cp
umount /etc/hostname
cp /etc/hostname_cp /etc/hostname
" > $DIR/$appname-$id/hack_scripts/hostfiles-hack.sh

  echo "4 Création du fichier preinstall.sh"
  echo "#!/bin/bash
DIRORI=$(dirname \$0)
cd \$DIRORI
# install type
INSTALL_TYPE=stable
[ \"\$1\" != \"\" ] && INSTALL_TYPE=\$1
# branche_type
BRANCHE_TYPE=buster
[ \"\$2\" != \"\" ] && BRANCHE_TYPE=\$2
# install requirements
# deprecated : --force-yes
apt-get update --quiet
apt-get install -y --no-install-recommends wget apt-utils ssh openssl ca-certificates openssh-server nano vim cron git
# debug docker context for resolvconf
# deprecated : --force-yes
apt-get install -y --no-install-recommends resolvconf 2>/dev/null || \\
 echo \"resolvconf resolvconf/linkify-resolvconf boolean false\" | debconf-set-selections
# get yunohost git repo
git clone -b \$BRANCHE_TYPE https://github.com/YunoHost/install_script /tmp/install_script
git -C /tmp/install_script checkout \$BRANCHE_TYPE
# hack YunoHost install_script for bypass systemd check
sed -i \"s@/run/systemd/system@/run@g\" /tmp/install_script/install_yunohost
# debug systemctl issues for docker, add proxy
mv /bin/systemctl /bin/systemctl.bin
echo -e \"#\"'!'\"/bin/sh\n/bin/./systemctl.bin\nexit 0\n\" > /bin/systemctl
chmod +x /bin/systemctl
# force ulimit for slapd now
ulimit -n 1024
# do yunohost installation
cd /tmp/install_script
./install_yunohost -f -a -d \$INSTALL_TYPE
[ \"\$?\" != \"0\" ] && echo \"error while yunohost installation\" && exit 1
# hack iptables for yunohost in docker
mv /usr/sbin/iptables /usr/sbin/iptables.ori
echo -e \"#\"'!'\"/bin/sh\necho \"fake iptables for yunohost inside docker, unusable. For return non failure unix code 0. Bye !\"\nexit 0\n\" > /usr/sbin/iptables
chmod +x /usr/sbin/iptables
# remove proxy for systemctl
rm -f /bin/systemctl
mv /bin/systemctl.bin /bin/systemctl
# force ulimit for slapd in starting script
sed -i '/\/lib\/lsb\/init-functions/a ulimit -n 1024' /etc/init.d/slapd
# patchs for yunohost
adduser admin
systemctl enable nginx
systemctl enable yunohost-api
systemctl enable php7.3-fpm
systemctl enable fail2ban
[ ! -f /etc/fail2ban/filter.d/sshd-ddos.conf ] \\
	&& echo -e \"[Definition]\n\" > /etc/fail2ban/filter.d/sshd-ddos.conf
[ ! -f /etc/fail2ban/filter.d/postfix-sasl.conf ] \\
        && echo -e \"[Definition]\n\" > /etc/fail2ban/filter.d/postfix-sasl.conf
touch /var/log/mail.log
#systemctl enable dovecot
#systemctl enable postfix
#systemctl enable rspamd
#systemctl enable avahi-daemon
#systemctl enable dnsmasq
#systemctl enable redis-server
# cleaning
apt-get clean
" > $DIR/$appname-$id/preinstall.sh
  
  echo "5 - Création du fichier Dockerfile "
echo "
FROM debian:10
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update && apt-get install -y systemd && apt-get clean
ADD preinstall.sh /tmp/preinstall.sh
RUN chmod +x /tmp/preinstall.sh
RUN /tmp/./preinstall.sh && rm /tmp/preinstall.sh && apt-get clean && apt-get autoclean
ADD hack_scripts/hostfiles-hack.sh /usr/local/bin/
ADD hack_scripts/hostfiles-hack.service /etc/systemd/system/
RUN chmod 664 /etc/systemd/system/hostfiles-hack.service && \
 chmod 744 /usr/local/bin/hostfiles-hack.sh && \
 systemctl enable hostfiles-hack.service
CMD [\"/bin/systemd\"]
"> $DIR/$appname-$id/Dockerfile

  echo "6 - Création du fichier $appname-$id.yml "
echo "
version: '3.6'
services:
  $appname-$id:
    build:
      context: .
    privileged: true
    container_name: $appname-$id
    restart: always
    ports:
      - $port22:22
      - $port25:25
      - $port53udp:53/udp
      - $port80:80
      - $port137:137
      - $port138:138
      - $port139:139
      - $port443:443
      - $port465:465
      - $port587:587
      - $port993:993
      - $port5222:5222
      - $port5269:5269
      - $port49200:49200
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - $appname-$id-media:/media
      - /sys/fs/cgroup:/sys/fs/cgroup:ro
      - $appname-$id-backup:/home/yunohost.backup
      - $appname-$id-log:/var/log/
    labels:
      - traefik.enable=true
      - traefik.http.routers.$appname-$id-frontend.rule=Host(\`$appname-$id.$domain\`)
      - traefik.http.routers.$appname-$id-frontend.entrypoints=secureweb
      - traefik.http.services.$appname-$id-frontend.loadbalancer.server.port=80
      - traefik.http.routers.$appname-$id-frontend.service=$appname-$id-frontend
      - traefik.http.routers.$appname-$id-frontend.tls.certresolver=cert
    hostname: $domain
    networks:
      frontend:
          aliases:
           - $domain
volumes:
  $appname-$id-media:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: ./$appname-$id/media
  $appname-$id-log:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: ./$appname-$id/log
  $appname-$id-backup:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: ./$appname-$id/backup
networks:
  frontend:
    external:
      name: $name_subnet
" > $DIR/$appname-$id/docker-compose.yml
  
  echo "4 - Lancement du conteneur $appname-$id "
  docker-compose -f $DIR/$appname-$id/docker-compose.yml up -d

  ip
}

# Let's Go !! parse args  ####################################################################

parse_options $@
