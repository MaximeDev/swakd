# Description

Script pour générer automatiquement des containers avec docker et docker compose  
permet de créer plusieurs container à la suite.  

Tester sur windows 64bit (amd64), ubuntu 64bit(amd64), raspberry os 64bit (arm64)

Ce script à été créer grâce au script generator de xavki :
https://gitlab.com/xavki/presentations-dockercompose/-/blob/master/21-utils-script-grafana/generator.sh

| Options               | Description                                          | windows |
| --------------------- | ---------------------------------------------------- | ------- |
| -h, --help            | affiche les commandes possibles                      |fonctionne|
| -a, --apapche         | lance un conteneur apache                            |fonctionne|
| -c, --cloner          | lance un conteneur cloner git                        |fonctionne|
| -ca, --cassandra      | lance un conteneur cassdrana                         |en test|
| -d, --drupal          | lance un conteneur drupal                            |fonctionne|
| -g, --gitlab          | lance un conteneur gitlab server                     |fonctionne|
| -gra, --grafana       | lance un conteneur grafana                           |fonctionne|
| -i, --ip              | liste les adresse ip pour chaque container           |fonctionne|
| -in, --init           | reinitialise tous (volumes,network,images,container) |fonctionne|
| -j, --jenkins         | lance un conteneur jenkins                           |fonctionne|
| -joo, --joomla        | lance un conteneur joomla                            |fonctionne|
| -k, --kafka           | lance un conteneur kafka avec zookeeper              |en test|
| -ma, --mariadb        | lance un conteneur mariadb                           |fonctionne|
| -mo, --mongodb        | lance un conteneur mongodb avec mongo-express        |fonctionne|
| -my, --mysql          | lance un conteneur mysql                             |fonctionne|
| -pma, --phpmyadmin    | lance un conteneur phpmyadmin                        |fonctionne|
| -po, --portainer      | lance un conteneur portainer                         |fonctionne|
| -pos, --postgres      | lance un conteneur postgres                          |fonctionne|
| -pro, --prometheus    | lance un conteneur prometheus                        |fonctionne|
| -r, --remove          | supprime tous les conteneur                          |fonctionne|
| -ri, --remove-images  | supprime toutes les images                           |fonctionne|
| -rn, --remove-network | supprime tous les réseaux                            |fonctionne|
| -rn, --remove-volumes | supprime tous les volumes                            |fonctionne|
| -t, --traefik         | lance un conteneur traefik                           |fonctionne|
| -s, --stop            | stop tous les conteneur en cours                     |fonctionne|
| -sy, --symfony        | lance un conteneur symfony avec lamp                 |fonctionne|
| -w, --wordpress       | lance un conteneur wordpress                         |fonctionne|
| -y ou --yunohost      | lance un conteneur yunohost                          |fonctionne|


## Installation pour windows

Télécharger git :
https://git-scm.com/downloads

Télécharger docker :
https://hub.docker.com/editions/community/docker-ce-desktop-windows/


### Instalation Git et docker-desktop avec  cmd et powershell
installe Git et docker-desktop, docker-compose et docker-cli depuis cmd
1) copier le code puis ouvrir cmd et coller le code, appuyer sur entrer
```bash
set dir=%cd% && "%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command Start-Process '%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe' -ArgumentList '-NoProfile -InputFormat None -ExecutionPolicy Bypass -Command [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12; iex ((New-Object System.Net.WebClient).DownloadString(''https://chocolatey.org/install.ps1'')); choco upgrade -y docker-desktop docker-cli docker-compose;choco upgrade -y git --params "/GitAndUnixToolsOnPath /NoAutoCrlf";  Read-Host ''Type ENTER to exit'' ' -Verb RunAs
```
2) ouvrir Docker-desktop pour qui fasse ses mises à jour

### clone du dépot et lancement du script

Faire un clique droit dans un dossier et selectionner "Git Bash Here" et cloner le depot du script
```bash
git clone https://gitlab.com/MaximeDev/swakd.git
```

Ensuite faire un clique droit dans le dossier télécharger et selectionner "Git Bash Here"  
!!Ce script est un script linux et ne se lance pas avec cmd ou powershell!!

puis faire la commande : (en n'oubliant pas le "./" avant le nom du script)
```bash
./swakd.sh -y
```

## Utilisation du script

pour help
```bash
./swakd.sh -h
```

pour lancer un container yunohost
```bash
./swakd.sh -y
```

pour lister les addresses ip et ports de chaque container
```bash
./swakd.sh -i
```

## réglage pour windows
pour utiliser avec windows il faut rajouter une route
1) pour cela on récupere le sous réseaux dans lequel est le container
Faire :
```bash
./swakd.sh -i
```
cela nous donne l'addresse ip du contener que l'on souhaite récuperer  
ex :"frontend//172.21.0.2"  
donc le sous réseaux de cette addresse sera : 172.21.0.0/16  

2) On récupere ensuite l'adresse ip addresse de notre pc 
avec cmd faire :
```bash
ipconfig
```
ex "Carte réseau sans fil Wi-Fi :  
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.2.130  "  
3) Ensuite on ajoute la route dans windows avec cmd avec les privileges administrateur
```bash
route add 172.21.0.0/16 192.168.2.130
```

## Installation pour linux

il faut avant installer git,docker and docker-compose  

https://git-scm.com/downloads
https://docs.docker.com/engine/install/

### Installation Debian
```bash
sudo apt update && apt upgrade
sudo apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common \
    git
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88
```
remplacer l'architecture en fonction de votre machine
[arch=amd64][arch=armhf] [arch=arm64]
```bash
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/debian \
   $(lsb_release -cs) \
   stable"
```
```bash
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
```
```bash
sudo curl -L "https://github.com/docker/compose/releases/download/1.27.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
```
### Installation Ubuntu
```bash
sudo apt update && apt upgrade
sudo apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common \
    git
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88
```
remplacer l'architecture en fonction de votre machine
[arch=amd64][arch=armhf] [arch=arm64]
```bash
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
```
```bash
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
```
### clone du dépot et lancement du script

```bash
git clone https://gitlab.com/MaximeDev/swakd.git
cd yunohost_script
chmod u+x swakd.sh
```

## Utilisation du script

pour help
```bash
sudo ./swakd.sh -h
```

pour lancer un container yunohost
```bash
sudo ./swakd.sh -y
```

pour lister les addresses ip et ports de chaque container
```bash
sudo ./swakd.sh -i
```